from django.urls import reverse_lazy 
from django.views.generic import CreateView, DeleteView, UpdateView, ListView, DetailView
from album.models import Team, Player

# Create your views here.
class TeamListView(ListView): #vista que trae todos los datos
    model = Team

class TeamDetailView(DetailView): #Trae un equipo especifico
    model = Team

class PlayerListView(ListView):
    model = Player

class PlayerDetailView(DetailView):
    model = Player
